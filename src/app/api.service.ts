/**
 * Let’s create a service that interfaces with the API. 
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


export interface Item{
  name: string;
  description: string;
  url: string;
  html: string;
  markdown: string;
}

@Injectable({
  providedIn: 'root'
})


export class ApiService {
  private dataURL:string = 'https://www.techiediaries.com/api/data.json';
  constructor(private httpClient: HttpClient) {}
  fetch(): Observable<Item[]>{ // é a declaração de um método que vai ser chamado depois
    return <Observable<Item[]>>this.httpClient.get(this.dataURL);
  }
}

/**
 * We first imported the HttpClient and Observable classes then injected the HttpClient in the 
 * constructor as httpClient and added a fetch() method which calls the get() method of HttpClient
 *  (for sending an HTTP GET request to our JSON endpoint) and returns an Observable that we can 
 * subscribe to later.
 * We also declared an Item interface which represents a single item of the returned JSON data.
 */

 /**
  * A service worker is a feature that’s available on modern browsers which can be used as a network proxy that 
  * lets your application intercept network requests to cache assets and data. This could be used for
  * implementing PWA features such as offline support and Push notifications etc.
  */