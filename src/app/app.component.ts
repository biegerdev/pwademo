//import this service from the application component
import { Component, OnInit } from '@angular/core';
import { ApiService } from './api.service' //export a class
import { Item } from './api.service'; //export an interface. Representa um unico item do JSON

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'pwademo';
  items: Array<Item>; //só uma declaração
  constructor(private apiService: ApiService){} //construtor recebe um apiService
  ngOnInit(){ //vai ser chamado quando AppComponent for iniciado 
    this.fetchData(); //chamo o método que estou declarando abaixo.
  }
  fetchData(){ //é um metodo que eu to declarando
    this.apiService.fetch().subscribe((data: Array<Item>) => {
      console.log(data);
      this.items = data;
    }, (err) =>{
      console.log(err);
    });
  }
}

/**
 * 
 * We import the ApiService that we created before and we inject it as apiService, we also import the 
 * Item class which represents a single item of our JSON data and we declare the items variable of type 
 * Array<Item> which will hold the fetched items.
 * 
 * Next, we add a fetchData() method which calls our fetch() method that we defined in the ApiService which 
 * returns an Observable. We simply subscribe to this observable in order to send a GET request to our JSON
 *  endpoint and get the response data that we finally assign to the items array.
 *
 * We call the fetchData() method in the ngOnInit() life-cycle event so it will be called once the AppComponent
 * component is initialized.
 * 
 */